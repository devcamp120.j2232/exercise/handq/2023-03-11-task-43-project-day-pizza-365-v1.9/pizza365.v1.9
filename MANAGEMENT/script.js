"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

var gOrderList = [];
var gOrderId = "";
var gOrderCode = "";

const gTABLE_ROW = ["id", "orderCode", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
const gSTT_COL = 0;
const gCODE_COL = 1;
const gCOMBO_COL = 2;
const gPIZZA_COL = 3;
const gDRINK_COL = 4;
const gTOTAL_COL = 5;
const gNAME_COL = 6;
const gPHONE_COL = 7;
const gSTATUS_COL = 8;
const gACTION_COL = 9;

var gStt = 1;

var gTableOrder = $("#order-table").DataTable({
    columns: [
        {data: gTABLE_ROW[gSTT_COL]},
        {data: gTABLE_ROW[gCODE_COL]},
        {data: gTABLE_ROW[gCOMBO_COL]},
        {data: gTABLE_ROW[gPIZZA_COL]},
        {data: gTABLE_ROW[gDRINK_COL]},
        {data: gTABLE_ROW[gTOTAL_COL]},
        {data: gTABLE_ROW[gNAME_COL]},
        {data: gTABLE_ROW[gPHONE_COL]},
        {data: gTABLE_ROW[gSTATUS_COL]},
        {data: gTABLE_ROW[gACTION_COL]},
    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            render: function() {
                return gStt++
            }
        },
        {
            targets: gACTION_COL,
            defaultContent: '<button id="btn-detail" class="btn btn-primary w-100">Detail</button>'
        },
    ]
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện tải trang/F5
$(document).ready(onPageLoading);

// gán sự kiện click button Filter
$(document).on("click", "#btn-filter", onBtnFilterClick);

// gán sự kiện click button Detail
$(document).on("click", "#btn-detail", onBtnDetailClick);
// gán sự kiện click button Confirm Order trên Modal Order Detail
$(document).on("click", "#btn-confirm", onBtnConfirmOrderClick);
// gán sự kiện click button Cancel Order trên Modal Order Detail
$(document).on("click", "#btn-cancel", onBtnCancelOrderClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sự kiện load trang/F5
function onPageLoading() {
    // Gọi API lấy danh sách đơn hàng và lưu vào biến toàn cục gOrderList
    apiGetOrderList();
    // Xử lý hiển thị
    insertOrderListToTable(gOrderList);
}

// function xử lý sự kiện click button Filter
function onBtnFilterClick() {
    // Khai báo đối tượng chứa dữ liệu filter
    let vFilterData = {
        trangThai: "",
        loaiPizza: "",
    };
    // Thu thập dữ liệu
    getFilterData(vFilterData);
    console.log(vFilterData);
    // Kiểm tra dữ liệu
    // Xử lý hiển thị
    let vResultData = getFilteredDataObject(vFilterData);
    insertOrderListToTable(vResultData);
}

// function xử lý sự kiện click button Detail (Chi tiết)
function onBtnDetailClick() {
    // hiện modal Order Detail
    $("#modal-order-detail").modal("show");
    // thu thập dữ liệu ID và Order code lưu vào biến toàn cục
    getUserDetailOnClick(this);
    // gọi API lấy thông tin đơn hàng bằng order code
    let vAjaxRequest = apiGetOrderDetailByOrderCode();
    vAjaxRequest.done(function(orderDetail) {
        // Xử lý hiển thị dữ liệu
        displayOrderDetail(orderDetail);
    });
}

// function xử lý sự kiện click button Confirm Order trên Modal Order Detail
function onBtnConfirmOrderClick() {
    // Thu thập dữ liệu trạng thái:
    let vOrderConfirm = {trangThai: "confirmed"};
    // Gọi API update đơn hàng
    let vAjaxRequest = apiUpdateOrder(vOrderConfirm);
    vAjaxRequest.done(function() {
        // Xử lý hiển thị
        handleConfirmOrderSuccess();
    });
}

// function xử lý sự kiện click button Cancel Order trên Modal Order Detail
function onBtnCancelOrderClick() {
    // Thu thập dữ liệu trạng thái:
    let vOrderCancel = {trangThai: "cancel"};
    // Gọi API update đơn hàng
    let vAjaxRequest = apiUpdateOrder(vOrderCancel);
    vAjaxRequest.done(function() {
        // Xử lý hiển thị
        handleCancelOrderSuccess();
    });
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// function gọi API lấy thông tin đơn hàng bằng order code
function apiGetOrderDetailByOrderCode() {
    return $.ajax({
        url:gBASE_URL + "/" + gOrderCode,
        type: "GET",
    });
}

// function gọi API lấy danh sách đơn hàng và lưu vào biến toàn cục gOrderList 
function apiGetOrderList() {
    return $.ajax({
        url: gBASE_URL,
        type: "GET",
        async: false,
    }).done(function(orderList) {
        gOrderList = orderList;
    }).fail(function(error) {
        console.log("Lấy dữ liệu Order List không thành công");
    });
}

// function gọi API để cập nhật trạng thái confirm hoặc cancel cho 01 order (update)
function apiUpdateOrder(paramStatus) {
    return $.ajax({
        url: gBASE_URL + "/" + gOrderId,
        type: "PUT",
        data: JSON.stringify(paramStatus),
        contentType: "application/json",
    });
}

// function insert danh sách đơn hàng vào table
function insertOrderListToTable(paramOrderList) {
    gStt = 1;
    gTableOrder.clear();
    gTableOrder.rows.add(paramOrderList);
    gTableOrder.draw();
}

// function thu thập dữ liệu filter
// function return tham số đối tượng paramFilterData chứa dữ liệu người dùng chọn trên form
function getFilterData(paramFilterData) {
    paramFilterData.trangThai = $("#select-status").val();
    paramFilterData.loaiPizza = $("#select-pizza").val();
}

// function xử lý filter data theo firstname và lastname
function getFilteredDataObject(paramFilterData) {
    let vFilteredData = gOrderList.filter(function(order) {
        return (order.trangThai.toLowerCase() == paramFilterData.trangThai.toLowerCase() || paramFilterData.trangThai == "") 
            && (order.loaiPizza.toLowerCase() == paramFilterData.loaiPizza.toLowerCase() || paramFilterData.loaiPizza == "");
    })
    return vFilteredData;
}

// function thu thập dữ liệu của Row khi click button Detail
function getUserDetailOnClick(paramButton) {
    let vRowClicked = $(paramButton).closest("tr");
    let vUserData = gTableOrder.row(vRowClicked).data();
    gOrderCode = vUserData.orderCode;
    gOrderId = vUserData.id;
}

// function hiển thị dữ liệu order detail lên Modal Order Detail
function displayOrderDetail(paramOrderDetail) {
    $("#input-ma-don").val(paramOrderDetail.orderCode);
    $("#input-kich-co").val(paramOrderDetail.kichCo);
    $("#input-duong-kinh").val(paramOrderDetail.duongKinh);
    $("#input-suon").val(paramOrderDetail.suon);
    $("#input-salad").val(paramOrderDetail.salad);
    $("#input-loai-pizza").val(paramOrderDetail.loaiPizza);
    $("#input-voucher").val(paramOrderDetail.idVourcher);
    $("#input-thanh-tien").val(paramOrderDetail.thanhTien);
    $("#input-giam-gia").val(paramOrderDetail.giamGia);
    $("#input-drink").val(paramOrderDetail.idLoaiNuocUong);
    $("#input-drink-number").val(paramOrderDetail.soLuongNuoc);
    $("#input-fullname").val(paramOrderDetail.hoTen);
    $("#input-email").val(paramOrderDetail.email);
    $("#input-phone").val(paramOrderDetail.soDienThoai);
    $("#input-address").val(paramOrderDetail.diaChi);
    $("#input-message").val(paramOrderDetail.loiNhan);
    $("#input-status").val(paramOrderDetail.trangThai);
    $("#input-day-create").val(paramOrderDetail.ngayTao);
    $("#input-day-modify").val(paramOrderDetail.ngayCapNhat);
}

// function xử lý hiển thị Confirm đơn hàng thành công
function handleConfirmOrderSuccess() {
    $("#modal-order-detail").modal("hide");
    $("#modal-confirm-order").modal("show");
    // Tải lại order list và tải lại table data
    apiGetOrderList();
    insertOrderListToTable(gOrderList);
}

// function xử lý hiển thị Cancel đơn hàng thành công
function handleCancelOrderSuccess() {
    $("#modal-order-detail").modal("hide");
    $("#modal-cancel-order").modal("show");
    // Tải lại order list và tải lại table data
    apiGetOrderList();
    insertOrderListToTable(gOrderList);
}

// Xóa đơn hàng theo điều kiện
$(document).on("click", "#btn-delete", function() {
    apiGetOrderList();
    for (let order of gOrderList) {
        if 
        (
            order.kichCo == "" || 
            order.loaiPizza == "" || 
            order.idLoaiNuocUong == "" || 
            order.thanhTien == 0 || 
            order.idLoaiNuocUong == "" || 
            order.hoTen == "" || 
            order.idLoaiNuocUong == "" || 
            order.soDienThoai == "" || 
            order.hoTen.length < 5 
            // order.soDienThoai.length < 7
        ) 
        {
            $.ajax({
                url: gBASE_URL + "/" + order.id,
                type: "DELETE",
            })
        }
}
})

// function gọi API tạo đơn hàng mới
$(document).on("click", "#btn-add", function() {
    for (let i = 1; i < 101; i++) {
        let vObjectRequest = {
            kichCo: "M",
            duongKinh: "25",
            suon: "4",
            salad: "300",
            loaiPizza: "BACON",
            idVourcher: "",
            idLoaiNuocUong: "FANTA",
            soLuongNuoc: "3",
            hoTen: "DEVCAMP " + i,
            thanhTien: "200000",
            email: "devcamp120@devcamp.edu.vn",
            soDienThoai: "0987654321",
            diaChi: "Quận Mười Ba",
            loiNhan: "Đế mỏng viền hải sản"
        };
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(vObjectRequest)
        });
    }
})
